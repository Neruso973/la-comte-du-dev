import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
@ApiTags('api')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiResponse({ type: String })
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
