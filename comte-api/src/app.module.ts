import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaService } from './prisma.service';
import { LogModule } from './routes/log/log.module';
import { SharedModule } from './shared/shared.module';

@Module({
  imports: [LogModule, SharedModule],
  controllers: [AppController],
  providers: [AppService, PrismaService],
})
export class AppModule {}
