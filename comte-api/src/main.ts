import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PrismaService } from './prisma.service';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({ origin: '*' });

  //Prisma
  const prismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);

  //Swagger
  const config = new DocumentBuilder()
    .setTitle('La Comté du Dev')
    .setDescription('API description')
    .setVersion('1.0')
    .addTag('La Comté')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/swagger', app, document);

  await app.listen(8080);
}
bootstrap();
