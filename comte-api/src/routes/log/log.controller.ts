import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Post,
  UsePipes,
} from '@nestjs/common';
import {
  ApiBody,
  ApiNotFoundResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { LogService } from './log.service';
import { LogViewModel } from './models/log.viewModel';
import { LogModel } from './models/log.model';
import { JoiValidationPipe } from 'src/shared/pipe/joi-validation.pipe';
import { logSchema } from 'src/shared/schemas/joi-log.schema';

@ApiTags('api/log')
@Controller('api/log')
export class LogController {
  constructor(private readonly _logService: LogService) {}

  @ApiResponse({ type: [LogViewModel] })
  @ApiNotFoundResponse({ description: 'No log yet' })
  @Get()
  async getLogs(): Promise<LogViewModel[]> {
    const logs = await this._logService.getLog();
    if (logs.length === 0) {
      throw new NotFoundException('No log yet');
    }
    return logs;
  }
  @UsePipes(new JoiValidationPipe(logSchema))
  @ApiBody({ type: LogModel })
  @ApiResponse({ type: LogViewModel })
  @Post()
  public async addLog(@Body() log: LogModel): Promise<LogModel> {
    const { ...data } = log;
    return this._logService.addLog({
      ...data,
    });
  }
}
