import { BadRequestException, Injectable } from '@nestjs/common';
import { CategoryEnum, Log, Prisma, SourceEnum } from '@prisma/client';
import { PrismaService } from '../../prisma.service';
import { LogModel } from './models/log.model';

@Injectable()
export class LogService {
  constructor(private _prisma: PrismaService) {}

  //Get all Log
  async getLog(): Promise<Log[]> {
    return this._prisma.log.findMany();
  }

  // add a log
  public async addLog(dto: LogModel): Promise<Log> {
    const data: Prisma.LogCreateInput = {
      component: dto.component,
      message: dto.message,
      error: dto?.error ?? 'error',
      category: dto.category,
      source: dto.source,
      date: new Date(),
    };

    const source = dto.source;
    const category = dto.category;

    if (!(source in SourceEnum && category in CategoryEnum)) {
      throw new BadRequestException('bad Enum');
    }

    return this._prisma.log.create({
      data,
    });
  }
}
