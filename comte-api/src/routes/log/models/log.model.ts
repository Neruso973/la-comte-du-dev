import { ApiProperty } from '@nestjs/swagger';
import { CategoryEnum, SourceEnum } from '@prisma/client';

export class LogModel {
  @ApiProperty()
  component: string;

  @ApiProperty()
  message: string;

  @ApiProperty()
  error: string;

  @ApiProperty()
  category: CategoryEnum;

  @ApiProperty()
  source: SourceEnum;
}
