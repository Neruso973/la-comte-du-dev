import { ApiProperty } from '@nestjs/swagger';
import { LogModel } from './log.model';

export class LogViewModel extends LogModel {
  @ApiProperty()
  id: number;

  @ApiProperty()
  date: Date;
}
