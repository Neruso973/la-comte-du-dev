import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private _messageService: MessageService) {}

  public successNotif(summary: string, detail: string) {
    this._messageService.add({
      severity: 'success',
      summary: summary,
      detail: detail,
    });
  }

  public errorNotif(summary: string, detail: string) {
    this._messageService.add({
      severity: 'error',
      summary: summary,
      detail: detail,
    });
  }

  public infoNotif(summary: string, detail: string) {
    this._messageService.add({
      severity: 'info',
      summary: summary,
      detail: detail,
    });
  }

  public warnNotif(summary: string, detail: string) {
    this._messageService.add({
      severity: 'warn',
      summary: summary,
      detail: detail,
    });
  }
}
