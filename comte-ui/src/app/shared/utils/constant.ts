export const REGEX = {
  PASSWORD_PATTERN: /^(?=.[a-z])(?=.[A-Z])(?=.[0-9])(?=.[!@#$%^&*])(?=.{8,})/g,
  LOWERCASE: /[a-z]/g,
  UPPERCASE: /[A-Z]/g,
  NUMBER: /[0-9]/g,
  SPECIAL_CARACTER: /[!@#$%^&*]/g,
  MAIL: /^[\w-.]+@([\w-]+.)+[\w-]{2,4}$/g,
};
